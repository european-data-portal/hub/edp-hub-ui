# EDP hub ui

## Project Setup

Install [Node.js v12.13.0+ and NPM 6.10.0+](https://nodejs.org/en/) on your system

Clone or download the code:

Install NPM packages:

    $ cd edp-hub-ui
    $ npm install

<br>

#### Build for Development

Open a terminal in the `edp-hub-ui` directory and run:

    $ npm run dev

<br>This will start a local webserver on Port `8080` (unless you defined a different port in `edp-hub-ui/config/index.js`).
<br>Open a web browser and visit `http://localhost:8080` to see the app.
<br>Hot Module Replacement is supported. The page will update automatically whenever files are changed and saved.

<br>

#### Build for Production

Open a terminal in the `edp-hub-ui` directory and run:

    $ npm run build

<br>This will optimize files for production and store the bundle in
  `edp-hub-ui/dist`
<br>Deploy the contents of `edp-hub-ui/dist` on your webserver.

<br>

#### Run it via Docker

- Build the application for production
```
$ docker build -t edp-hub-ui .
$ docker run -i -p 8080:8080 edp-hub-ui
```

<br>

## Configurations

The `edp-hub-ui` consists of several configuration files for users and developers.

#### User Configurations

**Note:** _Environment variables created by the [Runtime Configurations](#runtime-configurations) will always override the corresponding configurations from `user-config.js` when used correctly!_

##### user-config.js
The user-config.js file is located at `edp-hub-ui/config/` by default. It is the main project configuration file. The following example file shortly describes the configurable values.

<details>
<summary>Open user-config.js Example File</summary>

```javascript

const glueConfig = {
  title: 'European Data Portal',
  description: 'European Data Portal',
  keywords: 'EDP',
  api: {
    baseUrl: 'https://www.europeandataportal.eu/data/search/',
    similarityBaseUrl: 'https://www.europeandataportal.eu/api/similarities/',
    gazetteerBaseUrl: 'https://www.europeandataportal.eu/data/search/gazetteer/',
    uploadBaseUrl: 'https://www.europeandataportal.eu/data/api/',
    matomoUrl: 'https://www.europeandataportal.eu/piwik/',
    authToken: '',
  },
  keycloak: {
    enableLogin: true,
    realm: 'edp',
    url: 'https://www.europeandataportal.eu/auth',
    'ssl-required': 'external',
    clientId: 'edp-ui',
    'public-client': true,
    'verify-token-audience': true,
    'use-resource-role-mappings': true,
    'confidential-port': 0,
  },
  rtp: {
    grand_type: 'urn:ietf:params:oauth:grant-type:uma-ticket',
    audience: 'edp-hub-repo',
  },
  locale: 'en',
  fallbackLocale: 'en',
  services: {
    catalogueService,
    datasetService,
    distributionService,
    datastoreService,
    gazetteerService,
    uploadService,
    authService,
  },
  themes: {
    header: 'dark',
  },
  routerOptions: {
    base: '/data',
    mode: 'history',
  },
  navigation: {
    top: {
      main: {
        home: {
          // href: 'https://link-to-external-url.com' (optional)
          // target: ['_self' | '_blank'] (optional)
          show: true,
        },
        data: {
          show: true,
        },
        maps: {
          show: false,
        },
        about: {
          show: false,
        },
        append: [
          {
            href: 'https://www.fokus.fraunhofer.de/datenschutz',
            target: '_self',
            title: 'Privacy Policy',
          },
          {
            href: 'https://www.fokus.fraunhofer.de/9663f8cb2d267d4b',
            target: '_self',
            title: 'Imprint',
          },
        ],
        icons: false,
      },
      sub: {
        privacyPolicy: {
          show: false,
          href: 'https://www.fokus.fraunhofer.de/datenschutz',
          target: '_self',
        },
        imprint: {
          show: false,
          href: 'https://www.fokus.fraunhofer.de/9663f8cb2d267d4b',
          target: '_self',
        },
      },
    },
    bottom: {}
  },
  images: {
    top: [
      {
        src: 'https://i.imgur.com/lgtG4zB.png',
        // href: 'https://my-url.de'(optional)
        // target: ['_self' | '_blank'] (optional)
        description: 'Logo European data portal',
        height: '60px',
        width: 'auto',
      },
    ],
    bottom: [],
  },
};

```

</details>

##### runtime-config.js
The `runtime-config.js` file is located at `edp-hub-ui/config/` by default. It is a template file, which lists all configurable environment variables that can be changed during runtime. See [Runtime Configurations](#runtime-configurations) for more information.

##### i18n.json
The `i18n.json` file is located at `edp-hub-ui/config/i18n/` by default. It contains translations for all available languages.

##### UserImprint.vue / UserPrivacyPolicy.vue
The `UserImprint.vue` and `UserPrivacyPolicy.vue` files are located at `edp-hub-ui/src/components/user/`. They are more or less empty Vue components. Custom Imprint / PrivacyPolicy pages can be implemented here if no external pages are used (configurable in `user-config.js`).

<br>

#### Developer Configurations

##### index.js
The `index.js` file is located at `edp-hub-ui/config/` by default and is generated by the Vue-Webpack-Bundle. It contains several configurations for the development and production build process.

##### custom_theme.scss
The `custom-theme.scss` file is located at `edp-hub-ui/src/styles/` by default. It contains Bootstrap 4 SCSS variables and overrides the default Bootstrap values. It must be used to change any general styling rules like spacing, sizes, colors etc. It is also possible to add new color variables or other new variables, which can then be used via Bootstrap classes.

<br>

#### Runtime Configurations

**Note:** _Runtime Configurations are only applied, when running the application via [Docker](#run-it-via-docker)_

We utilize a Vue plugin `RuntimeConfiguration` to configure a web application using environment variables without rebuilding it.

See [runtime-config.js](config/runtime-config.js) for all available runtime variables.

##### Create new runtime variables

**Note:** _Just like the default configuration, runtime configurations (or environment variables) will be loaded client-side. Therefore, it is recommended that you **do not** store sensitive information like passwords or tokens._

Configuration variables are only changeable during runtime if bind to an environment variable by following specific rules.

To do so, follow these steps:
1. In [runtime-config.js](config/runtime-config.js), add the desired configuration variable as a property and enter it´s environment variable name as value. However, there are still some restrictions:
    -  While the variable name can be chosen freely, it must have the prefix `$VUE_APP`.
    -  The property should be consistent in it´s name *and* structure.
2.  Build and deploy the application.
3.  Set the environment variable *without* the dollar sign `$` at the beginning, e.g. if the new entry in [runtime-config.js](config/runtime-config.js) is `MATOMO_URL: '$VUE_APP_MATOMO_URL'`, make sure that the environment variable `VUE_APP_MATOMO_URL` is set accordingly.

##### Example

Let's suppose `process.env` looks like this (depending on how the project is set up):
```
{
  NODE_ENV: 'production',
  ROOT_API: 'https://www.europeandataportal.eu/api',
  ROOT_URL: 'https://www.europeandataportal.eu',
  MATOMO: {
      API_URL: 'https://www.europeandataportal.eu/piwik/',
  }
}
```

and we want to change `ROOT_API` and add a new property `MATOMO.ID` during runtime. Let's go through the steps outlined above:

1.  Add new property `MATOMO.ID` to [runtime-config.js](config/runtime-config.js):
```
export default {
  ROOT_API: '$VUE_APP_ROOT_API',
  ROOT_URL: '$VUE_APP_ROOT_URL',
  MATOMO: {
      API_URL: '$VUE_APP_MATOMO_API_URL',
      ID: '$VUE_APP_MATOMO_ID'
  }
}
```
2.  Build and deploy.
3.  Set the environment variables `VUE_APP_ROOT_API` and `VUE_APP_MATOMO_ID`:
```
VUE_APP_ROOT_API=https://www.europeandataportal.eu/newApi
VUE_APP_MATOMO_ID=89
```
