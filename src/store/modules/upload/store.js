/**
 * @author Hapreet Singh
 * @description Vuex store for data to use together with upload component
 */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const getDefaultState = () => ({
  step: 1,
  validate: false,
  error: '',
  id: '',
  title: '',
  description: '',
  keywords: [],
  category: [],
  distributions: [],
  contact: {
    type: '',
    name: '',
    email: '',
  },
  publisher: {
    type: '',
    name: '',
    homepage: '',
  },
  apiUploadData: '',
  catalogue: '',
  isLoading: false,
});

const state = getDefaultState();

const getters = {
  getStep: state => state.step,
  getValidate: state => state.validate,
  getError: state => state.error,
  getID: state => state.id,
  getTitle: state => state.title,
  getDescription: state => state.description,
  getKeywords: state => state.keywords,
  getCategory: state => state.category,
  getDistributions: state => state.distributions,
  getContactInfo: state => state.contact,
  getPublisherInfo: state => state.publisher,
  getAPIUploadData: state => state.apiUploadData,
  getCatalogue: state => state.catalogue,
  getLoading: state => state.isLoading,
};

const actions = {
  /**
   * @description Resets the store to the default state
   * @param commit
   */
  resetStore: ({ commit }) => {
    commit('RESET_STORE');
  },
  /**
   * @description Sets the step to which the user is currently switching.
   * @param commit
   * @param step - The step number.
   */
  setStep: ({ commit }, step) => {
    commit('SET_STEP', step);
  },
  /**
   * @description Sets the validate value to trigger the validation when using pagination next button.
   * @param commit
   * @param validate - The validate value (boolean).
   */
  setValidate: ({ commit }, validate) => {
    commit('SET_VALIDATE', validate);
  },
  /**
   * @description Sets the error message.
   * @param commit
   * @param error - The error message (String).
   */
  setError: ({ commit }, error) => {
    commit('SET_ERROR', error);
  },
  /**
   * @description Sets the isLoading value used for the loading spinner
   * @param commit
   * @param isLoading - The isLoading value (boolean).
   */
  setLoading: ({ commit }, isLoading) => {
    commit('SET_LOADING', isLoading);
  },
  /**
   * @description Sets the ID of the dataset
   * @param commit
   * @param validate - The dataset id.
   */
  setID: ({ commit }, id) => {
    commit('SET_ID', id);
  },
  /**
   * @description store the metadata details of the dataset distribution.
   * @param commit
   * @param dataset - The dataset request containing the form information .
   */
  addDatasetDetails: ({ commit }, dataset) => {
    commit('SET_TITLE', dataset.title);
    commit('SET_DESCRIPTION', dataset.description);
    commit('SET_KEYWORD', dataset.keywords);
    commit('SET_CATEGORY', dataset.category);
    commit('SET_CONTACT', dataset.contact);
    commit('SET_PUBLISHER', dataset.publisher);
    commit('SET_CATALOGUE', dataset.catalogue);
  },
  addDistribution: ({ commit }, distribution) => {
    commit('ADD_DISTRIBUTION', distribution);
  },
  setDistributions: ({ commit }, distributions) => {
    commit('SET_DISTRIBUTIONS', distributions);
  },
  addApiUploadData: ({ commit }, apiData) => {
    commit('ADD_API_UPLOAD_DATA', apiData);
  },
  resetUploadData: ({ commit }) => {
    commit('RESET_UPLOAD');
  },
};

const mutations = {
  RESET_STORE(state) {
    Object.assign(state, getDefaultState());
  },
  SET_STEP(state, step) {
    state.step = step;
  },
  SET_VALIDATE(state, validate) {
    state.validate = validate;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
  SET_LOADING(state, isLoading) {
    state.isLoading = isLoading;
  },
  SET_ID(state, id) {
    state.id = id;
  },
  SET_TITLE(state, title) {
    state.title = title;
  },
  SET_DESCRIPTION(state, description) {
    state.description = description;
  },
  SET_KEYWORD(state, keywords) {
    state.keywords = keywords;
  },
  SET_CATEGORY(state, category) {
    state.category = category;
  },
  SET_CONTACT(state, contact) {
    state.contact.name = contact.name;
    state.contact.type = contact.type;
    state.contact.email = contact.email;
  },
  SET_PUBLISHER(state, publisher) {
    state.publisher.name = publisher.name;
    state.publisher.type = publisher.type;
    state.publisher.homepage = publisher.homepage;
  },
  SET_CATALOGUE(state, catalogue) {
    state.catalogue = catalogue;
  },
  ADD_DISTRIBUTION(state, distribution) {
    state.distributions.push(distribution);
  },
  SET_DISTRIBUTIONS(state, distributions) {
    state.distributions = distributions;
  },
  ADD_API_UPLOAD_DATA(state, apiData) {
    state.apiUploadData = apiData;
  },
};

const module = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};

export default module;
